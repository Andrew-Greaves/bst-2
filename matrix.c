
#include <stddef.h>
#include "matrix.h"
#include<stdio.h>

//Matrix construction using bstree_ini()
Matrix matrix_construction(void){
    Matrix m=bstree_ini();
    return m;
}

//If location (index1,index2) is defined in matrix m, then return 1, else return 0
unsigned char matrix_isin(Matrix m,Index index1,Index index2){
    Key key=key_gen(index1,index2);
    Value *data=bstree_search(m,key);
    if(data!=NULL)
        return (char)1;
    else return (char) 0;
}

//If location(index1,index2) is defined in matrix m, then return a pointer to the associated data
Value *matrix_get(Matrix m,Index index1,Index index2){
    Key key=key_gen(index1,index2);
    Value *data=bstree_search(m,key);
    if(data!=NULL)
        return data;
    else return NULL;

}

//Assign value to matrix m at location (index1,index2). If that location already has value, then overwrite
void matrix_set(Matrix m,Index index1,Index index2,Value value){
    Key key=key_gen(index1,index2);
    Value *data=bstree_search(m,key);
    if(data!=NULL)
        data_set(data,value);
    else{
        data_gen(value);
        data_set(data,value);
    }
}

//If location (index1,index2) is defined in matrix m, then increase the associated valie by value
void matrix_inc(Matrix m,Index index1,Index index2,Value value){
    Key key=key_gen(index1,index2);
    Value *data=bstree_search(m,key);
    if(data!=NULL) {
        data_set(data,value+*data);
    }
    else printf("Error");
}

//Print indices and values in the matrix m
void matrix_list(Matrix m){
    bstree_traversal(m);
}

//Free allocated memory
void matrix_destruction(Matrix m){
    bstree_free(m);
}
