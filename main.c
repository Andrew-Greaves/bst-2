
#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include "matrix.h"

//Main function
int main() {
    Matrix matrix;
    matrix=matrix_construction();
    char string1[50],string2[50],line[50];
    int value=1;
    //Prompts user to enter two strings until a new line is entered
    printf("Enter two strings separated by a space ");
	while(fgets(line,50,stdin)!=NULL){
		if(line[0]=='\n')
			break;
		//Prints error message if user does not enter two strings
		else if(sscanf(line," %s %s",string1,string2)!=2)
			printf("Invalid number of entries\n");
		if(matrix_isin(matrix,string1,string2)==1)
			matrix_inc(matrix,string1,string2,1);
		else
			bstree_insert(matrix,key_gen(string1,string2),&value);
		printf("Enter two strings separtated by a space ");
	}
	printf("String 1\tString 2\tOccurences\n");
	matrix_list(matrix);
	matrix_destruction(matrix);	
}
