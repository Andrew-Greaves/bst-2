
#include "datatype.h"
#include <string.h>
#include <stdlib.h>
#include<stdio.h>

//Duplicates string pointed by str with dynamic allocation
char * string_dup(char *str){
    return strdup(str);
}

//Generate a key with dynamic memory allocation
Key key_gen(char *skey1,char *skey2){
    Key k;
    k=(Key )malloc(sizeof(Key));
    k->skey1=strdup(skey1);
    k->skey2=strdup(skey2);
    return k;
}

//Compares keys, returns -1 if key2 is bigger than key 1, 1 if key 1 is bigger than key2 and 0 if they are equal
int key_comp(Key key1,Key key2){
    if(strcmp(key1->skey1,key2->skey1)<0)
        return -1;
    else if(strcmp(key1->skey1,key2->skey1)>0)
        return 1;
    else if((strcmp(key1->skey1,key2->skey1)==0) && (strcmp(key1->skey2,key2->skey2)<0))
        return -1;
    else if ((strcmp(key1->skey1,key2->skey1)==0) && (strcmp(key1->skey2,key2->skey2)>0))
        return 1;
    else return 0;
}

//Print a key
void key_print(Key key){
    printf("%s\t\t",(key->skey1));
    printf("%s",(key->skey2));
}

//Free memory allocated for key
void key_free(Key key){
    free(key->skey1);
    free(key->skey2);
    free(key);
}

//Generate a data with dynamic allocation
Data data_gen(int idata){
    int *d;
    d=(int *)malloc(sizeof(int));
    return d;
}

//Assign data with idata
void data_set(Data data,int idata){
    *data=idata;
}

//Print data
void data_print(Data data){
    printf("\t\t%d\n",*data);
}

//Free memory allocated to data
void data_free(Data data){
    free(data);
}

