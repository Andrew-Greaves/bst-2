
#include "bstree.h"
#include<stddef.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>

//Allocate memory of type BStree_Node*, set the value NULL, and return a pointer to the allocated memory
BStree bstree_ini(void){
    BStree bst;
    bst=(BStree) malloc(sizeof(BStree_node*));
    *bst=NULL;
    return bst;
}

//Insert data with key into bst
void bstree_insert(BStree bst,Key key,Data data){
    Key key1=key_gen("","");
    Data data1=data_gen(0);
    if(*bst!=NULL){
      key1=(*bst)->key;
    }
    if(*bst==NULL){
      data_set(data1,*data);
      *bst=new_node(key,data1);
    }
    else if(key_comp(key1,key)==-1){
        bstree_insert(&(*bst)->right,key,data);
      }
    else if(key_comp(key1,key)==1){
        bstree_insert(&(*bst)->left,key,data);
      }
}

//Creates a node
BStree_node *new_node(Key key,Data data){
    BStree_node *node=(BStree_node*)malloc(sizeof(BStree_node));
    node->key=key;
    node->data=data;
    return node;
}

//If key is in bst, return key's data, else return NULL
Data bstree_search(BStree bst,Key key){
    Key key1=key_gen("","");
    if(*bst!=NULL)
    	 key1=(*bst)->key;
    if(*bst==NULL)
        return NULL;
    if(key_comp(key1,key)==0)
        return (*bst)->data;
    else if(key_comp(key1,key)==-1)
       return bstree_search(&(*bst)->right,key);
    else
        return bstree_search(&(*bst)->left,key);
}

//In order traversal of bst and print each node's key and data
void bstree_traversal(BStree bst){
    if(*bst==NULL)
        return;
    bstree_traversal(&(*bst)->left);
    key_print((*bst)->key);
    data_print((*bst)->data);
    bstree_traversal(&(*bst)->right);
}

//Free all memory 
void bstree_free(BStree bst){
    if(*bst==NULL)
	return;
    bstree_free(&(*bst)->left);
    bstree_free(&(*bst)->right);
    key_free((*bst)->key);
    data_free((*bst)->data);
    free(bst);
}
